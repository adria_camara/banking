<?php

// Define app routes

use Slim\App;
use Slim\Routing\RouteCollectorProxy;
use Tuupola\Middleware\HttpBasicAuthentication;

return function (App $app) {
    // Redirect to Swagger documentation
    $app->get('/', \App\Action\Home\HomeAction::class)->setName('home');

    $app->group(
        '/branches',
        function (RouteCollectorProxy $app) {
            $app->get('', \App\Action\Branch\BranchFindAction::class);
            $app->get('/create', \App\Action\Branch\BranchCreateAction::class);
            $app->post('/add', \App\Action\Branch\BranchAddAction::class);
            $app->get('/{branchId}', \App\Action\Branch\BranchReadAction::class);
            $app->post('/update/{branchId}', \App\Action\Branch\BranchUpdateAction::class);
            $app->post('/delete/{branchId}', \App\Action\Branch\BranchDeleteAction::class);
        }
    );

    $app->group(
        '/locations',
        function (RouteCollectorProxy $app) {
            $app->get('', \App\Action\Location\LocationFindAction::class);
            $app->get('/{branchId}', \App\Action\Location\LocationReadAction::class);
        }
    );

    $app->group(
        '/customers',
        function (RouteCollectorProxy $app) {
            $app->get('', \App\Action\Customer\CustomerFindAction::class);
            $app->get('/create', \App\Action\Customer\CustomerCreateAction::class);
            $app->post('/add', \App\Action\Customer\CustomerAddAction::class);
            $app->get('/{customerId}', \App\Action\Customer\CustomerReadAction::class);
            $app->post('/update/{customerId}', \App\Action\Customer\CustomerUpdateAction::class);
            $app->post('/delete/{customerId}', \App\Action\Customer\CustomerDeleteAction::class);
            $app->get('/createMovement/{customerId}', \App\Action\Customer\CustomerCreateMovementAction::class);
            $app->post('/doMovement/{customerId}', \App\Action\Customer\CustomerDoMovementAction::class);
            $app->get('/movementsList/{customerId}', \App\Action\Customer\CustomerMovementsListAction::class);
        }
    );

    $app->group(
        '/reports',
        function (RouteCollectorProxy $app) {
            $app->get('', \App\Action\Report\ReportFindAction::class);
        }
    );
};
