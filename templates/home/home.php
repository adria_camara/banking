<?php include_once __DIR__ . '/../layout/head.php' ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
                </div>

                <!-- Content Row -->
                <div class="row">

                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            Branches
                                        </div>
                                        <div class="row">
                                            <div class="text-info pl-2">
                                                <a href="/branches/create" class="text-info">
                                                    <i class="fas fa-fw fa-plus"></i>
                                                    Create new branch
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="pl-2">
                                                <a href="/branches" class="text-info">
                                                    <i class="fas fa-fw fa-list"></i>
                                                    List branches
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            Customers
                                        </div>
                                        <div class="row">
                                            <a href="/customers/create" class="text-info">
                                                <div class="text-info pl-2">
                                                    <i class="fas fa-fw fa-plus"></i>
                                                    Create new customer
                                                </div>
                                            </a>
                                        </div>
                                        <div class="row">
                                            <a href="/customers" class="text-info">
                                                <div class="text-info pl-2">
                                                    <i class="fas fa-fw fa-list"></i>
                                                    List customers
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-info shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            Reports
                                        </div>
                                        <div class="row">
                                            <div class="text-info pl-2">
                                                <i class="fas fa-fw fa-chart-line"></i>
                                                See reports
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!-- /.container-fluid -->



<?php include_once __DIR__ . '/../layout/footer.php' ?>

</body>

</html>
