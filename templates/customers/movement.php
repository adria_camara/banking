<?php

use App\Domain\Movement\Movement;
use App\Domain\Customer\Customer;

include_once __DIR__ . '/../layout/head.php'

/** @var Customer $customer */
?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">New movement for <?php echo $customer->getFullInfo(); ?></h1>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/customers/doMovement/<?php echo $customer->getId() ?>" method="post">
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                            <label for="type">Movement type</label>
                            <select class="custom-select js--movementType" id="type" name="type">
                                <option value=""> -- Select type -- </option>
                                <option value="<?php echo Movement::TYPE_ADD?>">Add money</option>
                                <option value="<?php echo Movement::TYPE_SUBTRACT?>">Subtract money</option>
                                <option value="<?php echo Movement::TYPE_TRANSFER?>">Transfer money</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-4 js--otherCustomers d-none">
                        <div class="form-group">
                            <label for="customerRelated">Customer</label>
                            <select class="custom-select" id="customerRelated" name="customerRelated">
                                <option value=""> -- Select type -- </option>
                                <?php
                                /** @var Customer $other */
                                foreach ($otherCustomers as $other):?>
                                    <option value="<?php echo $other->getId() ?>"> <?php echo $other->getFullInfo() ?> </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="amount">Amount</label>
                            <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="description">description</label>
                    <input type="text" class="form-control" id="description" name="description" placeholder="Movement description">
                </div>

                <div class="form-group float-right">
                    <a href="/customers" class="btn btn-secondary"><i class="fa fa-backward"></i> Back</a>
                    <input type="submit" class="btn btn-primary" value="Save movement">
                </div>
            </form>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

<?php include_once __DIR__ . '/../layout/footer.php' ?>

<script>
    $(document).ready(function () {
        $('.js--movementType').on('change', function () {
            if( $(this).val() == <?php echo Movement::TYPE_TRANSFER; ?> ) {
                $('.js--otherCustomers').removeClass('d-none');
            } else {
                $('.js--otherCustomers').addClass('d-none');
            }
        })
    });
</script>

</body>

</html>
