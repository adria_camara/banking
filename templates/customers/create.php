<?php

use App\Domain\Branch\Branch;
use App\Domain\Customer\Customer;

include_once __DIR__ . '/../layout/head.php' ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">New Customer</h1>
                </div>

                <div class="card shadow mb-4">
                    <div class="card-body">
                        <form action="/customers/add" method="post">
                            <div class="form-group">
                                <label for="locationId">Branch</label>
                                <select class="custom-select" id="branchId" name="branchId">
                                    <option> -- Select a branch -- </option>
                                    <?php
                                    /** @var Branch $location */
                                    foreach ($branches as $branch) : ?>
                                        <option value="<?php echo $branch->getId(); ?>">
                                            <?php echo $branch->getName() ; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Customer name">
                            </div>

                            <div class="form-group">
                                <label for="city">Surname</label>
                                <input type="text" class="form-control" id="surname" name="surname" placeholder="Customer surname">
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="code">Legal ID</label>
                                        <input type="text" class="form-control" id="legalId" name="legalId" placeholder="Legal ID">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="postalCode">Initial Balance</label>
                                        <input type="text" class="form-control" id="initialBalance" name="initialBalance" placeholder="Initial Balance">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group float-right">
                                <a href="/customers" class="btn btn-secondary"><i class="fa fa-backward"></i> Back</a>
                                <input type="submit" class="btn btn-primary" value="Save Customer">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->



<?php include_once __DIR__ . '/../layout/footer.php' ?>

</body>

</html>
