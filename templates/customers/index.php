<?php

use App\Domain\Branch\Branch;
use App\Domain\Customer\Customer;

include_once __DIR__ . '/../layout/head.php' ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Customers</h1>
                    <a href="/customers/create" class="btn btn-primary btn-icon-split btn-lg float-right">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus-circle"></i>
                        </span>
                        <span class="text">New customer</span>
                    </a>
                </div>

                <div class="card shadow mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Branch</th>
                                        <th>Name</th>
                                        <th>Initial balance</th>
                                        <th>Current balance</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Branch</th>
                                        <th>Name</th>
                                        <th>Initial balance</th>
                                        <th>Current balance</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php
                                    /** @var Customer $customer */
                                    foreach ($customers as $customer) :?>
                                        <tr>
                                            <td><?php echo $customer->getId(); ?></td>
                                            <td><?php echo $customer->getBranch()->getName(); ?></td>
                                            <td><?php echo $customer->getName(); ?> <?php echo $customer->getSurname(); ?></td>
                                            <td><?php echo number_format($customer->getInitialBalance(), 2, ',', '.'); ?> €</td>
                                            <td><?php echo number_format($customer->getBalance(), 2,',', '.'); ?> €</td>
                                            <td>
                                                <a href="/customers/createMovement/<?php echo $customer->getId();?>" class="text-info ml-2"
                                                   data-toggle="tooltip" data-placement="top" title="Create movement">
                                                    <i class="fa fa-random "></i>
                                                </a>
                                                <a href="/customers/movementsList/<?php echo $customer->getId();?>" class="text-info ml-2"
                                                   data-toggle="tooltip" data-placement="top" title="See movements">
                                                    <i class="fa fa-list "></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->



<?php include_once __DIR__ . '/../layout/footer.php' ?>

</body>

</html>
