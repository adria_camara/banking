<?php

use App\Domain\Movement\Movement;
use App\Domain\Customer\Customer;

/** @var Customer $customer */

include_once __DIR__ . '/../layout/head.php' ?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Movements for <?php echo $customer->getFullInfo()?></h1>
        <h3 class="h3 mb-0 text-gray-800">Current balance <?php echo number_format($customer->getBalance(), 2, ',', '.')?> €</h3>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Description</th>
                        <th>Customer related</th>
                        <th>Date</th>
                        <th>Balance</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Type</th>
                        <th>Amount</th>
                        <th>Description</th>
                        <th>Customer related</th>
                        <th>Date</th>
                        <th>Balance</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php
                    /** @var Movement $movement */
                    foreach ($movements as $movement) :?>
                        <tr>
                            <td><?php echo $movement->getTypeName(); ?></td>
                            <td class="<?php echo ($movement->getIsAddType()) ? 'text-success' : 'text-danger' ?>">
                                <?php echo number_format($movement->getAmount(), 2, ',', '.'); ?> €
                            </td>
                            <td><?php echo $movement->getDescription() ?></td>
                            <td><?php echo $movement->getCustomerRelatedName();?></td>
                            <td><?php echo $movement->getMovementDate()->format('d/m/Y H:i:s');?></td>
                            <td><?php echo number_format($movement->getBalance(), 2,',', '.'); ?> €</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->



<?php include_once __DIR__ . '/../layout/footer.php' ?>

</body>

</html>
