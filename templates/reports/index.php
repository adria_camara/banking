<?php

include_once __DIR__ . '/../layout/head.php' ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Reports</h1>
                </div>

                <div class="row">

                    <!-- Area Chart -->
                    <div class="col-12">
                        <div class="card shadow mb-4">
                            <!-- Card Header - Dropdown -->
                            <div
                                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                <h6 class="m-0 font-weight-bold text-primary">Highest balance by branch</h6>
                            </div>
                            <!-- Card Body -->
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Best balance</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($bestBranches as $branch) :?>
                                            <tr>
                                                <td><?php echo $branch['code']; ?></td>
                                                <td><?php echo $branch['name']; ?></td>
                                                <td><?php echo number_format($branch['balance'], 2); ?> €</td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Pie Chart -->
                    <div class="col-12">
                        <div class="card shadow mb-4">
                            <!-- Card Header - Dropdown -->
                            <div
                                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                <h6 class="m-0 font-weight-bold text-primary">Branches with customers balance over 50K</h6>
                            </div>
                            <!-- Card Body -->
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Best balance</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(empty($branchesOver50K)): ?>
                                            <tr>
                                                <td colspan="3"> No branches with more than 2 customers with balance over 50K.</td>
                                            </tr>
                                        <?php else : ?>
                                            <?php foreach ($branchesOver50K as $branch) :?>
                                                <tr>
                                                    <td><?php echo $branch['code']; ?></td>
                                                    <td><?php echo $branch['name']; ?></td>
                                                    <td><?php echo $branch['balance']; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->



<?php include_once __DIR__ . '/../layout/footer.php' ?>

</body>

</html>
