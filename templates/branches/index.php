<?php

use App\Domain\Branch\Branch;

include_once __DIR__ . '/../layout/head.php' ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Branches</h1>
                    <a href="/branches/create" class="btn btn-primary btn-icon-split btn-lg float-right">
                        <span class="icon text-white-50">
                            <i class="fas fa-plus-circle"></i>
                        </span>
                        <span class="text">New branch</span>
                    </a>
                </div>

                <div class="card shadow mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Code</th>
                                        <th>Location</th>
                                        <th>Name</th>
                                        <th>City</th>
                                        <th>Postal Code</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Code</th>
                                        <th>Location</th>
                                        <th>Name</th>
                                        <th>City</th>
                                        <th>Postal Code</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php
                                    /** @var Branch $branch */
                                    foreach ($branches as $branch) :?>
                                        <tr>
                                            <td><?php echo $branch->getCode(); ?></td>
                                            <td><?php echo $branch->getLocation()->getName(); ?></td>
                                            <td><?php echo $branch->getName(); ?></td>
                                            <td><?php echo $branch->getCity(); ?></td>
                                            <td><?php echo $branch->getPostalCode(); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->



<?php include_once __DIR__ . '/../layout/footer.php' ?>

</body>

</html>
