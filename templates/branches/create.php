<?php

use App\Domain\Branch\Branch;
use App\Domain\Location\Location;

include_once __DIR__ . '/../layout/head.php' ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">New Branch</h1>
                </div>

                <div class="card shadow mb-4">
                    <div class="card-body">
                        <form action="/branches/add" method="post">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Branch name">
                            </div>
                            <div class="form-group">
                                <label for="locationId">Location</label>
                                <select class="custom-select" id="locationId" name="locationId">
                                    <option> -- Select a location -- </option>
                                    <?php
                                    /** @var Location $location */
                                    foreach ($locations as $location) : ?>
                                        <option value="<?php echo $location->getId(); ?>">
                                            <?php echo $location->getName() . ' - ' . $location->getCountry() ; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="city">City</label>
                                <input type="text" class="form-control" id="city" name="city" placeholder="City">
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Branch address">
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="code">Internal Code</label>
                                        <input type="text" class="form-control" id="code" name="code" placeholder="Internal Code">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="postalCode">Postal Code</label>
                                        <input type="text" class="form-control" id="postalCode" name="postalCode" placeholder="Postal Code">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group float-right">
                                <a href="/branches" class="btn btn-secondary"><i class="fa fa-backward"></i> Back</a>
                                <input type="submit" class="btn btn-primary" value="Save Branch">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->



<?php include_once __DIR__ . '/../layout/footer.php' ?>

</body>

</html>
