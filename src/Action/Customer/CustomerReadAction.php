<?php

namespace App\Action\Customer;

use App\Domain\Customer\Service\CustomerReader;
use App\Responder\Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action.
 */
final class CustomerReadAction
{
    /**
     * @var CustomerReader
     */
    private $customerReader;

    /**
     * @var Responder
     */
    private $responder;

    /**
     * The constructor.
     *
     * @param CustomerReader $customerReader The service
     * @param Responder $responder The responder
     */
    public function __construct(CustomerReader $customerReader, Responder $responder)
    {
        $this->customerReader = $customerReader;
        $this->responder = $responder;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     * @param array<mixed> $args The routing arguments
     *
     * @return ResponseInterface The response
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        // Fetch parameters from the request
        $customerId = (int)$args['customerId'];

        // Invoke the domain (service class)
        $customer = $this->customerReader->getCustomerData($customerId);

        // Render the json response
        return $this->responder->withJson($response, $customer);
    }
}
