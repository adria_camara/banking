<?php

namespace App\Action\Customer;

use App\Domain\Customer\Service\CustomerReader;
use App\Domain\Movement\Service\MovementFinder;
use App\Responder\Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action.
 */
final class CustomerMovementsListAction
{
    /**
     * @var CustomerReader
     */
    private $customerReader;
    /**
     * @var MovementFinder
     */
    private $movementFinder;
    /**
     * @var Responder
     */
    private $responder;

    /**
     * The constructor.
     *
     * @param CustomerReader $customerIndex The user index list viewer
     * @param Responder $responder The responder
     */
    public function __construct(CustomerReader $customerIndex, MovementFinder $movementFinder, Responder $responder)
    {
        $this->customerReader = $customerIndex;
        $this->responder = $responder;
        $this->movementFinder = $movementFinder;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {

        $viewData = [
            'customer' => $this->customerReader->getCustomerData( $args['customerId'] ),
            'movements' => $this->movementFinder->findMovementsByCustomer( $args['customerId'] ),
        ];

        return $this->responder->withTemplate($response, 'customers/movementsList.php', $viewData);
    }
}
