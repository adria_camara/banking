<?php

namespace App\Action\Customer;

use App\Domain\Customer\Service\CustomerFinder;
use App\Domain\Customer\Service\CustomerReader;
use App\Responder\Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action.
 */
final class CustomerCreateMovementAction
{
    /**
     * @var Responder
     */
    private $responder;
    /** @var CustomerFinder  */
    private $customerFinder;
    /** @var CustomerReader $customerReader */
    private $customerReader;

    /**
     * CustomerCreateAction constructor.
     * @param Responder $responder
     * @param CustomerFinder $customerFinder
     */
    public function __construct(Responder $responder, CustomerFinder $customerFinder, CustomerReader $customerReader)
    {
        $this->responder = $responder;
        $this->customerFinder = $customerFinder;
        $this->customerReader = $customerReader;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $viewData = [
            'customer' => $this->customerReader->getCustomerData($args['customerId']),
            'otherCustomers' => $this->customerFinder->findOtherCustomers($args['customerId']),
        ];

        return $this->responder->withTemplate($response, 'customers/movement.php', $viewData);
    }
}
