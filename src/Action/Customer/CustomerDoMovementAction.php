<?php

namespace App\Action\Customer;

use App\Domain\Movement\Service\MovementCreator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;

/**
 * Action.
 */
final class CustomerDoMovementAction
{
    /**
     * @var App
     */
    private $app;

    /**
     * @var MovementCreator
     */
    private $movementCreator;

    /**
     * The constructor.
     *
     * @param App $responder The responder
     * @param MovementCreator $movementCreator The service
     */
    public function __construct(App $app, MovementCreator $movementCreator)
    {
        $this->app = $app;
        $this->movementCreator = $movementCreator;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        // Extract the form data from the request body
        $data = (array)$request->getParsedBody();
        $data['customerId'] = $args['customerId'];
        if(!isset($data['type']) || $data['type'] == '' ) {
            $data['type'] = null ;
        }
        if(!isset($data['customerRelated']) || $data['customerRelated'] == '' ) {
            $data['customerRelated'] = null ;
        }

        // Invoke the Domain with inputs and retain the result
        $customerId = $this->movementCreator->createMovement($data);

        return $response
            ->withHeader('Location', '/customers')
            ->withStatus(302);

    }
}
