<?php

namespace App\Action\Customer;

use App\Domain\Branch\Service\BranchFinder;
use App\Responder\Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action.
 */
final class CustomerCreateAction
{
    /**
     * @var Responder
     */
    private $responder;
    /** @var BranchFinder  */
    private $branchFinder;

    /**
     * CustomerCreateAction constructor.
     * @param Responder $responder
     * @param BranchFinder $branchFinder
     */
    public function __construct(Responder $responder, BranchFinder $branchFinder)
    {
        $this->responder = $responder;
        $this->branchFinder = $branchFinder;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $params = (array)$request->getQueryParams();

        $viewData = [
            'branches' => $this->branchFinder->findBranches($params),
        ];

        return $this->responder->withTemplate($response, 'customers/create.php', $viewData);
    }
}
