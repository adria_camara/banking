<?php

namespace App\Action\Customer;

use App\Domain\Customer\Service\CustomerCreator;
use App\Responder\Responder;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;

/**
 * Action.
 */
final class CustomerAddAction
{
    /**
     * @var App
     */
    private $app;

    /**
     * @var CustomerCreator
     */
    private $customerCreator;

    /**
     * The constructor.
     *
     * @param App $responder The responder
     * @param CustomerCreator $customerCreator The service
     */
    public function __construct(App $app, CustomerCreator $customerCreator)
    {
        $this->app = $app;
        $this->customerCreator = $customerCreator;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Extract the form data from the request body
        $data = (array)$request->getParsedBody();

        // Invoke the Domain with inputs and retain the result
        $customerId = $this->customerCreator->createCustomer($data);

        return $response
            ->withHeader('Location', '/customers')
            ->withStatus(302);

    }
}
