<?php

namespace App\Action\Customer;

use App\Domain\Customer\Service\CustomerFinder;
use App\Responder\Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action.
 */
final class CustomerFindAction
{
    /**
     * @var CustomerFinder
     */
    private $customerFinder;

    /**
     * @var Responder
     */
    private $responder;

    /**
     * The constructor.
     *
     * @param CustomerFinder $customerIndex The user index list viewer
     * @param Responder $responder The responder
     */
    public function __construct(CustomerFinder $customerIndex, Responder $responder)
    {
        $this->customerFinder = $customerIndex;
        $this->responder = $responder;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $params = (array)$request->getQueryParams();

        $viewData = [
            'customers' => $this->customerFinder->findCustomers($params),
        ];

        return $this->responder->withTemplate($response, 'customers/index.php', $viewData);
    }
}
