<?php

namespace App\Action\Branch;

use App\Domain\Branch\Service\BranchCreator;
use App\Domain\Location\Service\LocationFinder;
use App\Responder\Responder;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action.
 */
final class BranchCreateAction
{
    /**
     * @var Responder
     */
    private $responder;
    /** @var LocationFinder  */
    private $locationFinder;

    /**
     * The constructor.
     *
     * @param Responder $responder The responder
     */
    public function __construct(Responder $responder, LocationFinder $locationFinder)
    {
        $this->responder = $responder;
        $this->locationFinder = $locationFinder;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $params = (array)$request->getQueryParams();

        $viewData = [
            'locations' => $this->locationFinder->findLocations($params),
        ];

        return $this->responder->withTemplate($response, 'branches/create.php', $viewData);
    }
}
