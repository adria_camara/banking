<?php

namespace App\Action\Branch;

use App\Domain\Branch\Service\BranchCreator;
use App\Responder\Responder;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;

/**
 * Action.
 */
final class BranchAddAction
{
    /**
     * @var App
     */
    private $app;

    /**
     * @var BranchCreator
     */
    private $branchCreator;

    /**
     * The constructor.
     *
     * @param App $responder The responder
     * @param BranchCreator $branchCreator The service
     */
    public function __construct(App $app, BranchCreator $branchCreator)
    {
        $this->app = $app;
        $this->branchCreator = $branchCreator;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Extract the form data from the request body
        $data = (array)$request->getParsedBody();

        // Invoke the Domain with inputs and retain the result
        $branchId = $this->branchCreator->createBranch($data);

        return $response
            ->withHeader('Location', '/branches')
            ->withStatus(302);

    }
}
