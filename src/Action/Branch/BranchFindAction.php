<?php

namespace App\Action\Branch;

use App\Domain\Branch\Service\BranchFinder;
use App\Responder\Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action.
 */
final class BranchFindAction
{
    /**
     * @var BranchFinder
     */
    private $branchFinder;

    /**
     * @var Responder
     */
    private $responder;

    /**
     * The constructor.
     *
     * @param BranchFinder $branchIndex The user index list viewer
     * @param Responder $responder The responder
     */
    public function __construct(BranchFinder $branchIndex, Responder $responder)
    {
        $this->branchFinder = $branchIndex;
        $this->responder = $responder;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $params = (array)$request->getQueryParams();

        $viewData = [
            'branches' => $this->branchFinder->findBranches($params),
        ];

        return $this->responder->withTemplate($response, 'branches/index.php', $viewData);
    }
}
