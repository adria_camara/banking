<?php

namespace App\Action\Branch;

use App\Domain\Branch\Service\BranchReader;
use App\Responder\Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action.
 */
final class BranchReadAction
{
    /**
     * @var BranchReader
     */
    private $branchReader;

    /**
     * @var Responder
     */
    private $responder;

    /**
     * The constructor.
     *
     * @param BranchReader $branchReader The service
     * @param Responder $responder The responder
     */
    public function __construct(BranchReader $branchReader, Responder $responder)
    {
        $this->branchReader = $branchReader;
        $this->responder = $responder;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     * @param array<mixed> $args The routing arguments
     *
     * @return ResponseInterface The response
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        // Fetch parameters from the request
        $branchId = (int)$args['branchId'];

        // Invoke the domain (service class)
        $branch = $this->branchReader->getBranchData($branchId);

        // Render the json response
        return $this->responder->withJson($response, $branch);
    }
}
