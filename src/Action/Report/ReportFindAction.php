<?php

namespace App\Action\Report;

use App\Domain\Report\Service\ReportFinder;
use App\Responder\Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action.
 */
final class ReportFindAction
{
    /**
     * @var ReportFinder
     */
    private $reportFinder;

    /**
     * @var Responder
     */
    private $responder;

    /**
     * The constructor.
     *
     * @param ReportFinder $reportFinder
     * @param Responder $responder The responder
     */
    public function __construct(ReportFinder $reportFinder, Responder $responder)
    {
        $this->reportFinder = $reportFinder;
        $this->responder = $responder;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $bestByBranch = $this->reportFinder->getHighestByBranch();
        $branchesOver50K = $this->reportFinder->getBranchesOver50k();

        $dataBest = [
            'branch' => [],
            'balance' => [],
        ];
        $dataOver50 = [
            'branch' => [],
            'balance' => [],
        ];

        foreach ($bestByBranch as $indx => $branch) {

            $dataBest['branch'][] = $branch['name'];
            $dataBest['balance'][] = number_format($branch['balance'], 2, ',', '');
        }

        foreach ($branchesOver50K as $indx => $branch) {

            $dataOver50['branch'][] = $branch['name'];
            $dataOver50['balance'][] = number_format($branch['balance'], 2,',', '');
        }

        $viewData = [
            'bestBranches' => $bestByBranch,
            'dataBest' => $dataBest,
            'branchesOver50K' => $branchesOver50K,
            'dataOver50'=> $dataOver50,
        ];

        return $this->responder->withTemplate($response, 'reports/index.php', $viewData);
    }
}
