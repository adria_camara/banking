<?php

namespace App\Action\Location;

use App\Domain\Location\Service\LocationFinder;
use App\Responder\Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action.
 */
final class LocationFindAction
{
    /**
     * @var LocationFinder
     */
    private $locationFinder;

    /**
     * @var Responder
     */
    private $responder;

    /**
     * The constructor.
     *
     * @param LocationFinder $locationIndex The user index list viewer
     * @param Responder $responder The responder
     */
    public function __construct(LocationFinder $locationIndex, Responder $responder)
    {
        $this->branchFinder = $locationIndex;
        $this->responder = $responder;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $params = (array)$request->getQueryParams();

        $viewData = [
            'locations' => $this->branchFinder->findLocations($params),
        ];

        return $this->responder->withTemplate($response, 'locations/index.php', $viewData);
    }
}
