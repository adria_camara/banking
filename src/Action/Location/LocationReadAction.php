<?php

namespace App\Action\Location;

use App\Domain\Location\Service\LocationReader;
use App\Responder\Responder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Action.
 */
final class LocationReadAction
{
    /**
     * @var LocationReader
     */
    private $locationReader;

    /**
     * @var Responder
     */
    private $responder;

    /**
     * The constructor.
     *
     * @param LocationReader $locationReader The service
     * @param Responder $responder The responder
     */
    public function __construct(LocationReader $locationReader, Responder $responder)
    {
        $this->branchReader = $locationReader;
        $this->responder = $responder;
    }

    /**
     * Action.
     *
     * @param ServerRequestInterface $request The request
     * @param ResponseInterface $response The response
     * @param array<mixed> $args The routing arguments
     *
     * @return ResponseInterface The response
     */
    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        // Fetch parameters from the request
        $locationId = (int)$args['branchId'];

        // Invoke the domain (service class)
        $location = $this->branchReader->getLocationData($locationId);

        // Render the json response
        return $this->responder->withJson($response, $location);
    }
}
