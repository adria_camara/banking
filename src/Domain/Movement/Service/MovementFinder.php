<?php

namespace App\Domain\Movement\Service;

use App\Domain\Branch\Service\BranchReader;
use App\Domain\Customer\Service\CustomerReader;
use App\Domain\Movement\Repository\MovementFinderRepository;
use App\Domain\Movement\Movement;

/**
 * Service.
 */
final class MovementFinder
{
    /**
     * @var MovementFinderRepository
     */
    private $repository;

    /** @var BranchReader  */
    private $branchReader;

    /** @var CustomerReader */
    private $customerReader;

    /**
     * MovementFinder constructor.
     * @param Movement $movement
     * @param MovementFinderRepository $repository
     */
    public function __construct(MovementFinderRepository $repository, BranchReader $branchReader, CustomerReader $customerReader)
    {
        $this->repository = $repository;
        $this->branchReader = $branchReader;
        $this->customerReader = $customerReader;
    }

    /**
     * Find movements.
     *
     * @param array<mixed> $params The parameters
     *
     * @return array<mixed> The result
     */
    public function findMovements(array $params): array
    {
        $movementList = array();

        $movementResults = $this->repository->findMovements($params);

        foreach ($movementResults as $movementRow) {
            $movement = new Movement($this->branchReader);
            $movement->populate($movementRow);
            $movementList[]= $movement;
        }

        return $movementList;
    }

    public function findMovementsByCustomer(int $customerId): array
    {
        $movementList = array();

        $movementResults = $this->repository->findMovementsByCustomer($customerId);

        foreach ($movementResults as $movementRow) {
            $movement = new Movement($this->customerReader);
            $movement->populate($movementRow);
            $movementList[]= $movement;
        }

        return $movementList;
    }
}
