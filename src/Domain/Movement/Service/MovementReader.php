<?php

namespace App\Domain\Movement\Service;

use App\Domain\Movement\Movement;
use App\Domain\Movement\Repository\MovementFinderRepository;
use App\Domain\Movement\Repository\MovementReaderRepository;
use App\Domain\Location\Service\LocationReader;

/**
 * Service.
 */
final class MovementReader
{
    /**
     * @var MovementReaderRepository
     */
    private $repository;
    /** @var LocationReader  */
    private $locationReader;

    /**
     * MovementFinder constructor.
     * @param Movement $movement
     * @param MovementFinderRepository $repository
     */
    public function __construct(MovementReaderRepository $repository, LocationReader $locationReader)
    {
        $this->repository = $repository;
        $this->locationReader = $locationReader;
    }

    /**
     * Read a movement.
     *
     * @param int $movementId The movement id
     *
     * @return array The movement data
     */
    public function getMovementData(int $movementId): Movement
    {
        // Input validation
        // ...

        // Fetch data from the database
        $movementRow = $this->repository->getMovementById($movementId);

        $movement = new Movement($this->locationReader);
        $movement->populate($movementRow);

        return $movement;
    }
}
