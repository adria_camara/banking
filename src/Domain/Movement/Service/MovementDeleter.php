<?php

namespace App\Domain\Movement\Service;

use App\Domain\Movement\Repository\MovementDeleterRepository;

/**
 * Service.
 */
final class MovementDeleter
{
    /**
     * @var MovementDeleterRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param MovementDeleterRepository $repository The repository
     */
    public function __construct(MovementDeleterRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Delete user.
     *
     * @param int $userId The user id
     *
     * @return void
     */
    public function deleteMovement(int $userId): void
    {
        // Input validation
        // ...

        $this->repository->deleteMovementById($userId);
    }
}
