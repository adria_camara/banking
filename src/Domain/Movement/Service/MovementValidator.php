<?php

namespace App\Domain\Movement\Service;

use App\Domain\Movement\Repository\MovementValidatorRepository;
use App\Domain\Movement\Type\MovementRoleType;
use App\Factory\ValidationFactory;
use Cake\Validation\Validator;
use Selective\Validation\Exception\ValidationException;

/**
 * Service.
 */
final class MovementValidator
{
    /**
     * @var MovementValidatorRepository
     */
    private $repository;

    /**
     * @var ValidationFactory
     */
    private $validationFactory;

    /**
     * The constructor.
     *
     * @param MovementValidatorRepository $repository The repository
     * @param ValidationFactory $validationFactory The validation
     */
    public function __construct(MovementValidatorRepository $repository, ValidationFactory $validationFactory)
    {
        $this->repository = $repository;
        $this->validationFactory = $validationFactory;
    }

    /**
     * Create validator.
     *
     * @return Validator The validator
     */
    private function createValidator(): Validator
    {
        $validator = $this->validationFactory->createValidator();

        return $validator
            ->notEmptyString('customerId', 'Input required')
            ->integer('customerId', 'Customer has a not valid id')
            ->notEmptyString('type', 'Input required')
            ->notEmptyString('amount', 'Input required');
    }

    /**
     * Validate new movement.
     *
     * @param array<mixed> $data The data
     *
     * @throws ValidationException
     *
     * @return void
     */
    public function validateMovement(array $data): void
    {
        $validator = $this->createValidator();

        $validationResult = $this->validationFactory->createResultFromErrors(
            $validator->validate($data)
        );

        if ($validationResult->fails()) {
            throw new ValidationException('Please check your input', $validationResult);
        }
    }

    /**
     * Validate update.
     *
     * @param int $movementId The movement id
     * @param array<mixed> $data The data
     *
     * @return void
     */
    public function validateMovementUpdate(int $movementId, array $data): void
    {
        if (!$this->repository->existsMovementId($movementId)) {
            throw new ValidationException(sprintf('Movement not found: %s', $movementId));
        }

        $this->validateMovement($data);
    }
}
