<?php

namespace App\Domain\Movement\Service;

use App\Domain\Customer\Service\CustomerReader;
use App\Domain\Customer\Service\CustomerUpdater;
use App\Domain\Movement\Movement;
use App\Domain\Movement\Repository\MovementCreatorRepository;
use App\Domain\Movement\Type\MovementRoleType;
use App\Factory\LoggerFactory;
use Psr\Log\LoggerInterface;

/**
 * Service.
 */
final class MovementCreator
{
    /**
     * @var MovementCreatorRepository
     */
    private $repository;

    /**
     * @var MovementValidator
     */
    private $movementValidator;
    /**
     * @var CustomerReader
     */
    private $customerReader;
    /**
     * @var CustomerUpdater
     */
    private $customerUpdater;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The constructor.
     *
     * @param MovementCreatorRepository $repository The repository
     * @param MovementValidator $movementValidator The validator
     * @param LoggerFactory $loggerFactory The logger factory
     */
    public function __construct(
        MovementCreatorRepository $repository,
        MovementValidator $movementValidator,
        CustomerReader $customerReader,
        CustomerUpdater $customerUpdater,
        LoggerFactory $loggerFactory
    ) {
        $this->repository = $repository;
        $this->movementValidator = $movementValidator;
        $this->logger = $loggerFactory
            ->addFileHandler('movement_creator.log')
            ->createLogger();

        $this->customerReader = $customerReader;
        $this->customerUpdater = $customerUpdater;
    }

    /**
     * Create a new movement.
     *
     * @param array<mixed> $data The form data
     *
     * @return int The new movement ID
     */
    public function createMovement(array $data): int
    {
        $customer = $this->customerReader->getCustomerData($data['customerId']);

        // Input validation
        $this->movementValidator->validateMovement($data);

        // Map form data to row
        $movementRow = $this->mapToMovementRow($data);

        $balance = $this->calculateBalance($data['type'], $customer->getBalance(), (float)$movementRow['amount']);

        $movementRow['balance'] = $balance;

        // Insert movement
        $movementId = $this->repository->insertMovement($movementRow);

        if($movementId) {
            $balanceData = [
                'balance' => $balance
            ];
            $this->customerUpdater->updateCustomer($customer->getId(), $balanceData );

            if( !is_null($data['customerRelated']) ) {
                $related = $this->customerReader->getCustomerData($data['customerRelated']);

                $relatedBalance = $this->calculateBalance(Movement::TYPE_TRANSFER_ADD, $related->getBalance(), (float)$movementRow['amount']);

                $movementRow['customer_id'] = $data['customerRelated'];
                $movementRow['type'] = Movement::TYPE_TRANSFER_ADD;
                $movementRow['customer_related'] = $data['customerId'];
                $movementRow['balance'] = $relatedBalance;

                $movementId = $this->repository->insertMovement($movementRow);
                $this->customerUpdater->updateCustomer($related->getId(), ['balance' => $relatedBalance ]);
            }
        }

        // Logging
        $this->logger->info(sprintf('Movement created successfully: %s', $movementId));

        return $movementId;
    }

    /**
     * Map data to row.
     *
     * @param array<mixed> $data The data
     *
     * @return array<mixed> The row
     */
    private function mapToMovementRow(array $data): array
    {
        $amount = str_replace(',','.', $data['amount']);

        $movementData = [
            'customer_id' => $data['customerId'],
            'description' => $data['description'],
            'type' => $data['type'],
            'amount' => $amount,
        ];

        if( !is_null($data['customerRelated']) ) {
            $movementData['customer_related'] = $data['customerRelated'];
        }

        return $movementData;
    }

    private function calculateBalance($type, $balance, $amount)
    {
        switch ($type){
            case Movement::TYPE_ADD :
            case Movement::TYPE_TRANSFER_ADD :

                return $balance + $amount;

            case Movement::TYPE_SUBTRACT :
            case Movement::TYPE_TRANSFER :

                return $balance - $amount;

            default:
                return $balance;

        }
    }
}
