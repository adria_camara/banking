<?php

namespace App\Domain\Movement\Repository;

use App\Domain\Movement\Service\MovementReader;
use App\Factory\QueryFactory;

/**
 * Repository.
 */
final class MovementValidatorRepository
{
    /**
     * @var QueryFactory The query factory
     */
    private $movementReader;


    public function __construct(MovementReader $movementReader)
    {
        $this->movementReader = $movementReader;
    }

    /**
     * Check movement id.
     *
     * @param int $movementId The movement id
     *
     * @return bool True if exists
     */
    public function existsMovementId(int $movementId): bool
    {
        return (bool)$this->movementReader->getMovementData($movementId);
    }
}
