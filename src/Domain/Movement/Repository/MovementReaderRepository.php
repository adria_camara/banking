<?php

namespace App\Domain\Movement\Repository;

use PDO;
use DomainException;

/**
 * Repository.
 */
final class MovementReaderRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get movement by id.
     *
     * @param int $movementId The movement id
     *
     * @throws DomainException
     *
     * @return array<mixed> The movement row
     */
    public function getMovementById(int $movementId): array
    {
        $sql = "SELECT * FROM movements WHERE id = :id";

        /** @var \PDOStatement $stmt */
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(':id', $movementId);
        $stmt->execute();
        $row = $stmt->fetch();

        if (!$row) {
            throw new DomainException(sprintf('Movement not found: %s', $movementId));
        }

        return $row;
    }
}
