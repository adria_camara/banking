<?php

namespace App\Domain\Movement\Repository;

use PDO;
use Cake\Chronos\Chronos;

/**
 * Repository.
 */
final class MovementCreatorRepository
{
    private $connection;


    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $row
     * @return int
     */
    public function insertMovement(array $row): int
    {
        $fields = '';
        $params = '';

        foreach ( $row as $key => $value) {

            $fields .= ($fields != '' ) ? ' ,' . $key : $key;
            $params .=  ($params != '' ) ? ' ,:' . $key : ':'.$key;
        }

        $sql= "INSERT INTO movements ($fields) VALUES ($params)";

        $stmt = $this->connection->prepare($sql);

        foreach ( $row as $key => $value) {
            $stmt->bindValue(':'.$key, $value);
        }

        $stmt->execute();

        return $this->connection->lastInsertId();
    }
}
