<?php

namespace App\Domain\Movement\Repository;

use PDO;

/**
 * Repository.
 */
final class MovementFinderRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Load data table entries.
     *
     * @param array<mixed> $params The user
     *
     * @return array<mixed> The list view data
     */
    public function findMovements(array $params): array
    {
        $sql = "SELECT * FROM movements";

        /** @var \PDOStatement $stmt */
        $stmt = $this->connection->query($sql);

        return $stmt->fetchAll();
    }

    public function findMovementsByCustomer(int $customerId): array
    {
        $sql = "SELECT * FROM movements where customer_id = :customerId ORDER BY movement_date DESC";

        /** @var \PDOStatement $stmt */
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(':customerId', $customerId);

        $stmt->execute();

        return $stmt->fetchAll();
    }
}
