<?php


namespace App\Domain\Movement;

use App\Domain\Customer\Service\CustomerReader;


class Movement implements \JsonSerializable
{
    const TYPE_ADD = 1;
    const TYPE_SUBTRACT = 2;
    const TYPE_TRANSFER = 3;
    const TYPE_TRANSFER_ADD = 4;

    private $id;
    private $customerId;
    private $customerRelated;
    private $amount;
    private $balance;
    private $description;
    private $type;
    private $movementDate;

    /** @var CustomerReader */
    private $customerReader;

    /**
     * Movement constructor.
     * @param CustomerReader $customerReader
     */
    public function __construct(CustomerReader $customerReader)
    {
        $this->customerReader = $customerReader;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param mixed $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getMovementDate()
    {
        return $this->movementDate;
    }

    /**
     * @param mixed $movementDate
     */
    public function setMovementDate($movementDate)
    {
        $this->movementDate = $movementDate;
    }


    public function populate($row)
    {

        if(isset($row['id']) && !empty($row['id'])) {
            $this->id = $row['id'];
        }

        if(isset($row['customer_id']) && !empty($row['customer_id'])) {
            $this->customerId = $row['customer_id'];
        }

        if(isset($row['customer_related']) && !empty($row['customer_related'])) {
            $this->customerRelated = $row['customer_related'];
        }

        if(isset($row['amount']) && !empty($row['amount'])) {
            $this->amount = $row['amount'];
        }

        if(isset($row['type']) && !empty($row['type'])) {
            $this->type = $row['type'];
        }

        if(isset($row['description']) && !empty($row['description'])) {
            $this->description = $row['description'];
        }

        if(isset($row['balance']) && !empty($row['balance'])) {
            $this->balance = $row['balance'];
        }

        if(isset($row['movement_date']) && !empty($row['movement_date'])) {
            $this->movementDate = new \DateTime($row['movement_date']);
        }

    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'customerId' => $this->customerId,
            'customerName' => $this->getCustomer()->getName(),
            'customerRelated' => $this->customerRelated,
            'customerRelatedName' => $this->getCustomerRelated()->getName(),
            'type' => $this->type,
            'amount' => $this->amount,
            'description' => $this->description,
            'balance' => $this->balance,
            'movementDate' => $this->movementDate->format('d/m/Y H:i:s'),
        ];
    }

    public function getCustomer()
    {
        return $this->customerReader->getCustomerData($this->customerId);
    }

    public function getCustomerRelated()
    {
        if(!is_null($this->customerRelated) ){
            return $this->customerReader->getCustomerData($this->customerRelated);
        }
        return $this->customerRelated;
    }

    public function getTypeName()
    {
        switch ($this->type) {
            case self::TYPE_ADD :
                return 'Add';
            case self::TYPE_SUBTRACT:
                return 'Subtract';
            case self::TYPE_TRANSFER:
            case self::TYPE_TRANSFER_ADD:
                return 'Transfer';
            default:
                return $this->type;
        }
    }

    public function getIsAddType()
    {
        switch ($this->type) {

            case self::TYPE_ADD :
            case self::TYPE_TRANSFER_ADD:
                return true ;

            case self::TYPE_SUBTRACT:
            case self::TYPE_TRANSFER:
                return false;

            default:
                return false;
        }
    }

    public function getCustomerRelatedName()
    {
        if( !is_null($this->customerRelated) ){
            return $this->getCustomerRelated()->getFullInfo();
        }
        return null;
    }
}
