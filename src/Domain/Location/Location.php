<?php


namespace App\Domain\Location;


class Location implements \JsonSerializable
{

    private $id;
    private $name;
    private $country;
    private $createdAt;
    private $updatedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function populate($row)
    {

        if(isset($row['id']) && !empty($row['id'])) {
            $this->id = $row['id'];
        }

        if(isset($row['name']) && !empty($row['name'])) {
            $this->name = $row['name'];
        }

        if(isset($row['country']) && !empty($row['country'])) {
            $this->country = $row['country'];
        }

        if(isset($row['created_at']) && !empty($row['created_at'])) {
            $this->createdAt = new \DateTime($row['created_at']);
        }

        if(isset($row['updated_at']) && !empty($row['updated_at'])) {
            $this->updatedAt = new \DateTime($row['updated_at']);
        }
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'country' => $this->country,
            'createdAt' => $this->createdAt->format('d/m/Y H:i:s'),
            'updatedAt' => $this->updatedAt->format('d/m/Y H:i:s'),
        ];
    }
}
