<?php

namespace App\Domain\Location\Service;

use App\Domain\Location\Location;
use App\Domain\Location\Repository\LocationReaderRepository;

/**
 * Service.
 */
final class LocationReader
{
    /**
     * @var LocationReaderRepository
     */
    private $repository;
    /**
     * @var Location
     */
    private $location;

    /**
     * LocationReader constructor.
     * @param Location $location
     * @param LocationReaderRepository $repository
     */
    public function __construct(Location $location, LocationReaderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Read a location.
     *
     * @param int $locationId The location id
     *
     * @return array The location data
     */
    public function getLocationData(int $locationId): Location
    {
        // Input validation
        // ...

        // Fetch data from the database
        $locationRow = $this->repository->getLocationById($locationId);

        $location = new Location();
        $location->populate($locationRow);

        return $location;
    }
}
