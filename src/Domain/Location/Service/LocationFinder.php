<?php

namespace App\Domain\Location\Service;

use App\Domain\Location\Repository\LocationFinderRepository;
use App\Domain\Location\Location;

/**
 * Service.
 */
final class LocationFinder
{
    /**
     * @var LocationFinderRepository
     */
    private $repository;

    /**
     * @var Location
     */
    private $location;

    /**
     * LocationFinder constructor.
     * @param Location $location
     * @param LocationFinderRepository $repository
     */
    public function __construct(Location $location, LocationFinderRepository $repository)
    {
        $this->repository = $repository;
        $this->location = $location;
    }

    /**
     *
     * @param array<mixed> $params The parameters
     *
     * @return array<mixed> The result
     */
    public function findLocations(array $params): array
    {
        $locationList = array();

        $locationResults = $this->repository->findLocations($params);

        foreach ($locationResults as $locationRow) {
            $location = new Location();
            $location->populate($locationRow);
            $locationList[]= $location;
        }

        return $locationList;
    }
}
