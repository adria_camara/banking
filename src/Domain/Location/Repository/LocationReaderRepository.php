<?php

namespace App\Domain\Location\Repository;

use PDO;
use DomainException;

/**
 * Repository.
 */
final class LocationReaderRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get location by id.
     *
     * @param int $locationId The location id
     *
     * @throws DomainException
     *
     * @return array<mixed> The location row
     */
    public function getLocationById(int $locationId): array
    {
        $sql = "SELECT * FROM locations WHERE id = :id";

        /** @var \PDOStatement $stmt */
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(':id', $locationId);
        $stmt->execute();
        $row = $stmt->fetch();

        if (!$row) {
            throw new DomainException(sprintf('Location not found: %s', $locationId));
        }

        return $row;
    }
}
