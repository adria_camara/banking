<?php

namespace App\Domain\Location\Repository;

use PDO;

/**
 * Repository.
 */
final class LocationFinderRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Load data table entries.
     *
     * @param array<mixed> $params The user
     *
     * @return array<mixed> The list view data
     */
    public function findLocations(array $params): array
    {
        $sql = "SELECT * FROM locations";

        /** @var \PDOStatement $stmt */
        $stmt = $this->connection->query($sql);

        return $stmt->fetchAll();
    }
}
