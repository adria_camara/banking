<?php

namespace App\Domain\Branch\Repository;

use App\Domain\Branch\Service\BranchReader;
use App\Factory\QueryFactory;

/**
 * Repository.
 */
final class BranchValidatorRepository
{
    /**
     * @var QueryFactory The query factory
     */
    private $branchReader;


    public function __construct(BranchReader $branchReader)
    {
        $this->branchReader = $branchReader;
    }

    /**
     * Check branch id.
     *
     * @param int $branchId The branch id
     *
     * @return bool True if exists
     */
    public function existsBranchId(int $branchId): bool
    {
        return (bool)$this->branchReader->getBranchData($branchId);
    }
}
