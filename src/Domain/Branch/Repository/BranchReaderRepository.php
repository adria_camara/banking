<?php

namespace App\Domain\Branch\Repository;

use PDO;
use DomainException;

/**
 * Repository.
 */
final class BranchReaderRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get branch by id.
     *
     * @param int $branchId The branch id
     *
     * @throws DomainException
     *
     * @return array<mixed> The branch row
     */
    public function getBranchById(int $branchId): array
    {
        $sql = "SELECT * FROM branches WHERE id = :id";

        /** @var \PDOStatement $stmt */
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(':id', $branchId);
        $stmt->execute();
        $row = $stmt->fetch();

        if (!$row) {
            throw new DomainException(sprintf('Branch not found: %s', $branchId));
        }

        return $row;
    }
}
