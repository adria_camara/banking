<?php

namespace App\Domain\Branch\Repository;

use PDO;
use Cake\Chronos\Chronos;

/**
 * Repository.
 */
final class BranchCreatorRepository
{
    private $connection;


    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $row
     * @return int
     */
    public function insertBranch(array $row): int
    {
        $sql= "INSERT INTO branches (location_id, name, address, city, code, postal_code) 
            VALUES (:location_id, :name, :address, :city, :code, :postal_code)";


        $stmt = $this->connection->prepare($sql);

        foreach ( $row as $key => $value) {
            $stmt->bindValue(':'.$key, $value);
        }

        $stmt->execute();

        return $this->connection->lastInsertId();
    }
}
