<?php

namespace App\Domain\Branch\Repository;

use PDO;

/**
 * Repository.
 */
final class BranchFinderRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Load data table entries.
     *
     * @param array<mixed> $params The user
     *
     * @return array<mixed> The list view data
     */
    public function findBranches(array $params): array
    {
        $sql = "SELECT * FROM branches";

        /** @var \PDOStatement $stmt */
        $stmt = $this->connection->query($sql);

        return $stmt->fetchAll();
    }
}
