<?php


namespace App\Domain\Branch;

use App\Domain\Location\Service\LocationReader;


class Branch implements \JsonSerializable
{

    private $id;
    private $locationId;
    private $name;
    private $city;
    private $adress;
    private $postalCode;
    private $code;
    private $createdAt;
    private $updatedAt;
    /** @var LocationReader */
    private $locationReader;

    /**
     * Branch constructor.
     * @param LocationReader $locationReader
     */
    public function __construct(LocationReader $locationReader)
    {
        $this->locationReader = $locationReader;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param mixed $locationId
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @param mixed $adress
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function populate($row)
    {

        if(isset($row['id']) && !empty($row['id'])) {
            $this->id = $row['id'];
        }

        if(isset($row['location_id']) && !empty($row['location_id'])) {
            $this->locationId = $row['location_id'];
        }

        if(isset($row['name']) && !empty($row['name'])) {
            $this->name = $row['name'];
        }

        if(isset($row['city']) && !empty($row['city'])) {
            $this->city = $row['city'];
        }

        if(isset($row['adress']) && !empty($row['adress'])) {
            $this->adress = $row['adress'];
        }

        if(isset($row['postal_code']) && !empty($row['postal_code'])) {
            $this->postalCode = $row['postal_code'];
        }

        if(isset($row['code']) && !empty($row['code'])) {
            $this->code = $row['code'];
        }

        if(isset($row['created_at']) && !empty($row['created_at'])) {
            $this->createdAt = new \DateTime($row['created_at']);
        }

        if(isset($row['updated_at']) && !empty($row['updated_at'])) {
            $this->updatedAt = new \DateTime($row['updated_at']);
        }
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'locationId' => $this->locationId,
            'locationName' => $this->getLocation()->getName(),
            'name' => $this->name,
            'city' => $this->city,
            'adress' => $this->adress,
            'postalCode' => $this->postalCode,
            'code' => $this->code,
            'createdAt' => $this->createdAt->format('d/m/Y H:i:s'),
            'updatedAt' => $this->updatedAt->format('d/m/Y H:i:s'),
        ];
    }

    public function getLocation()
    {
        return $this->locationReader->getLocationData($this->locationId);
    }
}
