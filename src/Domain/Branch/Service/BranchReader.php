<?php

namespace App\Domain\Branch\Service;

use App\Domain\Branch\Branch;
use App\Domain\Branch\Repository\BranchFinderRepository;
use App\Domain\Branch\Repository\BranchReaderRepository;
use App\Domain\Location\Service\LocationReader;

/**
 * Service.
 */
final class BranchReader
{
    /**
     * @var BranchReaderRepository
     */
    private $repository;
    /** @var LocationReader  */
    private $locationReader;

    /**
     * BranchFinder constructor.
     * @param Branch $branch
     * @param BranchFinderRepository $repository
     */
    public function __construct(BranchReaderRepository $repository, LocationReader $locationReader)
    {
        $this->repository = $repository;
        $this->locationReader = $locationReader;
    }

    /**
     * Read a branch.
     *
     * @param int $branchId The branch id
     *
     * @return array The branch data
     */
    public function getBranchData(int $branchId): Branch
    {
        // Input validation
        // ...

        // Fetch data from the database
        $branchRow = $this->repository->getBranchById($branchId);

        $branch = new Branch($this->locationReader);
        $branch->populate($branchRow);

        return $branch;
    }
}
