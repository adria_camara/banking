<?php

namespace App\Domain\Branch\Service;

use App\Domain\Branch\Repository\BranchFinderRepository;
use App\Domain\Branch\Branch;
use App\Domain\Location\Service\LocationReader;

/**
 * Service.
 */
final class BranchFinder
{
    /**
     * @var BranchFinderRepository
     */
    private $repository;

    /** @var LocationReader  */
    private $locationReader;

    /**
     * BranchFinder constructor.
     * @param Branch $branch
     * @param BranchFinderRepository $repository
     */
    public function __construct(BranchFinderRepository $repository, LocationReader $locationReader)
    {
        $this->repository = $repository;
        $this->locationReader = $locationReader;
    }

    /**
     * Find users.
     *
     * @param array<mixed> $params The parameters
     *
     * @return array<mixed> The result
     */
    public function findBranches(array $params): array
    {
        $branchList = array();

        $branchResults = $this->repository->findBranches($params);

        foreach ($branchResults as $branchRow) {
            $branch = new Branch($this->locationReader);
            $branch->populate($branchRow);
            $branchList[]= $branch;
        }

        return $branchList;
    }
}
