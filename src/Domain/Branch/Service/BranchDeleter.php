<?php

namespace App\Domain\Branch\Service;

use App\Domain\Branch\Repository\BranchDeleterRepository;

/**
 * Service.
 */
final class BranchDeleter
{
    /**
     * @var BranchDeleterRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param BranchDeleterRepository $repository The repository
     */
    public function __construct(BranchDeleterRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Delete user.
     *
     * @param int $userId The user id
     *
     * @return void
     */
    public function deleteBranch(int $userId): void
    {
        // Input validation
        // ...

        $this->repository->deleteBranchById($userId);
    }
}
