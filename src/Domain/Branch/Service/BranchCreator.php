<?php

namespace App\Domain\Branch\Service;

use App\Domain\Branch\Repository\BranchCreatorRepository;
use App\Domain\Branch\Type\BranchRoleType;
use App\Factory\LoggerFactory;
use Psr\Log\LoggerInterface;

/**
 * Service.
 */
final class BranchCreator
{
    /**
     * @var BranchCreatorRepository
     */
    private $repository;

    /**
     * @var BranchValidator
     */
    private $branchValidator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The constructor.
     *
     * @param BranchCreatorRepository $repository The repository
     * @param BranchValidator $branchValidator The validator
     * @param LoggerFactory $loggerFactory The logger factory
     */
    public function __construct(
        BranchCreatorRepository $repository,
        BranchValidator $branchValidator,
        LoggerFactory $loggerFactory
    ) {
        $this->repository = $repository;
        $this->branchValidator = $branchValidator;
        $this->logger = $loggerFactory
            ->addFileHandler('branch_creator.log')
            ->createLogger();
    }

    /**
     * Create a new branch.
     *
     * @param array<mixed> $data The form data
     *
     * @return int The new branch ID
     */
    public function createBranch(array $data): int
    {
        // Input validation
        $this->branchValidator->validateBranch($data);

        // Map form data to row
        $branchRow = $this->mapToBranchRow($data);

        // Insert branch
        $branchId = $this->repository->insertBranch($branchRow);

        // Logging
        $this->logger->info(sprintf('Branch created successfully: %s', $branchId));

        return $branchId;
    }

    /**
     * Map data to row.
     *
     * @param array<mixed> $data The data
     *
     * @return array<mixed> The row
     */
    private function mapToBranchRow(array $data): array
    {
        return [
            'name' => $data['name'],
            'city' => $data['city'],
            'address' => $data['address'],
            'code' => $data['code'],
            'postal_code' => $data['postalCode'],
            'location_id' => $data['locationId'],
        ];
    }
}
