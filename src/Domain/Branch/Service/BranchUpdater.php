<?php

namespace App\Domain\Branch\Service;

use App\Domain\Branch\Repository\BranchUpdaterRepository;
use App\Factory\LoggerFactory;
use Psr\Log\LoggerInterface;

/**
 * Service.
 */
final class BranchUpdater
{
    /**
     * @var BranchUpdaterRepository
     */
    private $repository;

    /**
     * @var BranchValidator
     */
    private $userValidator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The constructor.
     *
     * @param BranchUpdaterRepository $repository The repository
     * @param BranchValidator $userValidator The validator
     * @param LoggerFactory $loggerFactory The logger factory
     */
    public function __construct(
        BranchUpdaterRepository $repository,
        BranchValidator $userValidator,
        LoggerFactory $loggerFactory
    ) {
        $this->repository = $repository;
        $this->userValidator = $userValidator;
        $this->logger = $loggerFactory
            ->addFileHandler('user_updater.log')
            ->createLogger();
    }

    /**
     * Update user.
     *
     * @param int $userId The user id
     * @param array<mixed> $data The request data
     *
     * @return void
     */
    public function updateBranch(int $userId, array $data): void
    {
        // Input validation
        $this->userValidator->validateBranchUpdate($userId, $data);

        // Map form data to row
        $userRow = $this->mapToBranchRow($data);

        // Insert user
        $this->repository->updateBranch($userId, $userRow);

        // Logging
        $this->logger->info(sprintf('Branch updated successfully: %s', $userId));
    }

    /**
     * Map data to row.
     *
     * @param array<mixed> $data The data
     *
     * @return array<mixed> The row
     */
    private function mapToBranchRow(array $data): array
    {
        $result = [];

        if (isset($data['username'])) {
            $result['username'] = (string)$data['username'];
        }

        if (isset($data['password'])) {
            $result['password'] = (string)password_hash($data['password'], PASSWORD_DEFAULT);
        }

        if (isset($data['email'])) {
            $result['email'] = (string)$data['email'];
        }

        if (isset($data['first_name'])) {
            $result['first_name'] = (string)$data['first_name'];
        }

        if (isset($data['last_name'])) {
            $result['last_name'] = (string)$data['last_name'];
        }

        if (isset($data['user_role_id'])) {
            $result['user_role_id'] = (int)$data['user_role_id'];
        }

        if (isset($data['locale'])) {
            $result['locale'] = (string)$data['locale'];
        }

        if (isset($data['enabled'])) {
            $result['enabled'] = (int)$data['enabled'];
        }

        return $result;
    }
}
