<?php

namespace App\Domain\Branch\Service;

use App\Domain\Branch\Repository\BranchValidatorRepository;
use App\Domain\Branch\Type\BranchRoleType;
use App\Factory\ValidationFactory;
use Cake\Validation\Validator;
use Selective\Validation\Exception\ValidationException;

/**
 * Service.
 */
final class BranchValidator
{
    /**
     * @var BranchValidatorRepository
     */
    private $repository;

    /**
     * @var ValidationFactory
     */
    private $validationFactory;

    /**
     * The constructor.
     *
     * @param BranchValidatorRepository $repository The repository
     * @param ValidationFactory $validationFactory The validation
     */
    public function __construct(BranchValidatorRepository $repository, ValidationFactory $validationFactory)
    {
        $this->repository = $repository;
        $this->validationFactory = $validationFactory;
    }

    /**
     * Create validator.
     *
     * @return Validator The validator
     */
    private function createValidator(): Validator
    {
        $validator = $this->validationFactory->createValidator();

        return $validator
            ->notEmptyString('name', 'Input required')
            ->notEmptyString('city', 'Input required')
            ->notEmptyString('address', 'Input required')
            ->notEmptyString('code', 'Input required')
            ->notEmptyString('postalCode', 'Input required')
            ->notEmptyString('locationId', 'Input required');
    }

    /**
     * Validate new branch.
     *
     * @param array<mixed> $data The data
     *
     * @throws ValidationException
     *
     * @return void
     */
    public function validateBranch(array $data): void
    {
        $validator = $this->createValidator();

        $validationResult = $this->validationFactory->createResultFromErrors(
            $validator->validate($data)
        );

        if ($validationResult->fails()) {
            throw new ValidationException('Please check your input', $validationResult);
        }
    }

    /**
     * Validate update.
     *
     * @param int $branchId The branch id
     * @param array<mixed> $data The data
     *
     * @return void
     */
    public function validateBranchUpdate(int $branchId, array $data): void
    {
        if (!$this->repository->existsBranchId($branchId)) {
            throw new ValidationException(sprintf('Branch not found: %s', $branchId));
        }

        $this->validateBranch($data);
    }
}
