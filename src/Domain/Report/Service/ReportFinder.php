<?php

namespace App\Domain\Report\Service;

use PDO;
/**
 * Service.
 */
final class ReportFinder
{
    /**
     * @var PDO
     */
    private $connection;

    /**
     * ReportFinder constructor.
     * @param PDO $connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return array
     */
    public function getHighestByBranch(): array
    {

        $sql = "SELECT b.id as id, b.name as name, b.code as code, MAX(c.balance) as balance
            FROM branches b 
            LEFT JOIN customers c ON c.branch_id = b.id
            GROUP BY b.id";

        $stmt = $this->connection->query($sql);

        return $stmt->fetchAll();
    }

    public function getBranchesOver50k(): array
    {
        $sql = "SELECT b.id as id, b.name as name, b.code as code, MAX(c.balance) as balance
            FROM branches b 
            INNER JOIN customers c ON c.branch_id = b.id
            WHERE (SELECT COUNT(id) FROM customers WHERE branch_id = b.id AND balance >= 50000 ) > 2
            GROUP BY b.id";

        $stmt = $this->connection->query($sql);

        return $stmt->fetchAll();
    }

    public function getMovementsByCustomer($customerId):array
    {

    }
}
