<?php


namespace App\Domain\Customer;

use App\Domain\Branch\Service\BranchReader;


class Customer implements \JsonSerializable
{

    private $id;
    private $branchId;
    private $name;
    private $surname;
    private $legalId;
    private $initialBalance;
    private $balance;
    private $createdAt;
    private $updatedAt;
    /** @var BranchReader */
    private $branchReader;

    /**
     * Customer constructor.
     * @param BranchReader $branchReader
     */
    public function __construct(BranchReader $branchReader)
    {
        $this->branchReader = $branchReader;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getBranchId()
    {
        return $this->branchId;
    }

    /**
     * @param mixed $branchId
     */
    public function setBranchId($branchId)
    {
        $this->branchId = $branchId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getLegalId()
    {
        return $this->legalId;
    }

    /**
     * @param mixed $legalId
     */
    public function setLegalId($legalId)
    {
        $this->legalId = $legalId;
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getInitialBalance()
    {
        return $this->initialBalance;
    }

    /**
     * @param mixed $initialBalance
     */
    public function setInitialBalance($initialBalance)
    {
        $this->initialBalance = $initialBalance;
    }


    public function populate($row)
    {

        if(isset($row['id']) && !empty($row['id'])) {
            $this->id = $row['id'];
        }

        if(isset($row['branch_id']) && !empty($row['branch_id'])) {
            $this->branchId = $row['branch_id'];
        }

        if(isset($row['name']) && !empty($row['name'])) {
            $this->name = $row['name'];
        }

        if(isset($row['surname']) && !empty($row['surname'])) {
            $this->surname = $row['surname'];
        }

        if(isset($row['legal_id']) && !empty($row['legal_id'])) {
            $this->legalId = $row['legal_id'];
        }

        if(isset($row['balance']) && !empty($row['balance'])) {
            $this->balance = $row['balance'];
        }

        if(isset($row['initial_balance']) && !empty($row['initial_balance'])) {
            $this->initialBalance = $row['initial_balance'];
        }

        if(isset($row['created_at']) && !empty($row['created_at'])) {
            $this->createdAt = new \DateTime($row['created_at']);
        }

        if(isset($row['updated_at']) && !empty($row['updated_at'])) {
            $this->updatedAt = new \DateTime($row['updated_at']);
        }
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'branchId' => $this->branchId,
            'branchName' => $this->getBranch()->getName(),
            'name' => $this->name,
            'surname' => $this->surname,
            'legatId' => $this->legalId,
            'initialBalance' => $this->initialBalance,
            'balance' => $this->balance,
            'createdAt' => $this->createdAt->format('d/m/Y H:i:s'),
            'updatedAt' => $this->updatedAt->format('d/m/Y H:i:s'),
        ];
    }

    public function getBranch()
    {
        return $this->branchReader->getBranchData($this->branchId);
    }

    public function getFullInfo()
    {
        return $this->name . ' ' . $this->surname . ' - ' . $this->legalId ;
    }
}
