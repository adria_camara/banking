<?php

namespace App\Domain\Customer\Service;

use App\Domain\Customer\Repository\CustomerCreatorRepository;
use App\Domain\Customer\Type\CustomerRoleType;
use App\Factory\LoggerFactory;
use Psr\Log\LoggerInterface;

/**
 * Service.
 */
final class CustomerCreator
{
    /**
     * @var CustomerCreatorRepository
     */
    private $repository;

    /**
     * @var CustomerValidator
     */
    private $customerValidator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The constructor.
     *
     * @param CustomerCreatorRepository $repository The repository
     * @param CustomerValidator $customerValidator The validator
     * @param LoggerFactory $loggerFactory The logger factory
     */
    public function __construct(
        CustomerCreatorRepository $repository,
        CustomerValidator $customerValidator,
        LoggerFactory $loggerFactory
    ) {
        $this->repository = $repository;
        $this->customerValidator = $customerValidator;
        $this->logger = $loggerFactory
            ->addFileHandler('customer_creator.log')
            ->createLogger();
    }

    /**
     * Create a new customer.
     *
     * @param array<mixed> $data The form data
     *
     * @return int The new customer ID
     */
    public function createCustomer(array $data): int
    {
        // Input validation
        $this->customerValidator->validateCustomer($data);

        // Map form data to row
        $customerRow = $this->mapToCustomerRow($data);

        // Insert customer
        $customerId = $this->repository->insertCustomer($customerRow);

        // Logging
        $this->logger->info(sprintf('Customer created successfully: %s', $customerId));

        return $customerId;
    }

    /**
     * Map data to row.
     *
     * @param array<mixed> $data The data
     *
     * @return array<mixed> The row
     */
    private function mapToCustomerRow(array $data): array
    {
        $balance = str_replace(',','.', $data['initialBalance']);
        return [
            'name' => $data['name'],
            'surname' => $data['surname'],
            'branch_id' => $data['branchId'],
            'legal_id' => $data['legalId'],
            'initial_balance' => $balance,
            'balance' => $balance,
        ];
    }
}
