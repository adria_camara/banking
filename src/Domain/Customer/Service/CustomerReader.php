<?php

namespace App\Domain\Customer\Service;

use App\Domain\Customer\Customer;
use App\Domain\Customer\Repository\CustomerFinderRepository;
use App\Domain\Customer\Repository\CustomerReaderRepository;
use App\Domain\Branch\Service\BranchReader;

/**
 * Service.
 */
final class CustomerReader
{
    /**
     * @var CustomerReaderRepository
     */
    private $repository;
    /** @var BranchReader  */
    private $branchReader;

    /**
     * CustomerFinder constructor.
     * @param Customer $customer
     * @param CustomerFinderRepository $repository
     */
    public function __construct(CustomerReaderRepository $repository, BranchReader $branchReader)
    {
        $this->repository = $repository;
        $this->branchReader = $branchReader;
    }

    /**
     * Read a customer.
     *
     * @param int $customerId The customer id
     *
     * @return array The customer data
     */
    public function getCustomerData(int $customerId): Customer
    {
        // Input validation
        // ...

        // Fetch data from the database
        $customerRow = $this->repository->getCustomerById($customerId);

        $customer = new Customer($this->branchReader);
        $customer->populate($customerRow);

        return $customer;
    }
}
