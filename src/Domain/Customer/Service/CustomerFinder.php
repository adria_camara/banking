<?php

namespace App\Domain\Customer\Service;

use App\Domain\Branch\Service\BranchReader;
use App\Domain\Customer\Repository\CustomerFinderRepository;
use App\Domain\Customer\Customer;

/**
 * Service.
 */
final class CustomerFinder
{
    /**
     * @var CustomerFinderRepository
     */
    private $repository;

    /** @var BranchReader  */
    private $branchReader;

    /**
     * CustomerFinder constructor.
     * @param Customer $customer
     * @param CustomerFinderRepository $repository
     */
    public function __construct(CustomerFinderRepository $repository, BranchReader $branchReader)
    {
        $this->repository = $repository;
        $this->branchReader = $branchReader;
    }

    /**
     * Find customers.
     *
     * @param array<mixed> $params The parameters
     *
     * @return array<mixed> The result
     */
    public function findCustomers(array $params): array
    {
        $customerList = array();

        $customerResults = $this->repository->findCustomers($params);

        foreach ($customerResults as $customerRow) {
            $customer = new Customer($this->branchReader);
            $customer->populate($customerRow);
            $customerList[]= $customer;
        }

        return $customerList;
    }

    public function findOtherCustomers(int $customerId): array
    {
        $customerList = array();

        $customerResults = $this->repository->findOtherCustomers($customerId);

        foreach ($customerResults as $customerRow) {
            $customer = new Customer($this->branchReader);
            $customer->populate($customerRow);
            $customerList[]= $customer;
        }

        return $customerList;
    }
}
