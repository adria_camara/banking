<?php

namespace App\Domain\Customer\Service;

use App\Domain\Customer\Repository\CustomerValidatorRepository;
use App\Domain\Customer\Type\CustomerRoleType;
use App\Factory\ValidationFactory;
use Cake\Validation\Validator;
use Selective\Validation\Exception\ValidationException;

/**
 * Service.
 */
final class CustomerValidator
{
    /**
     * @var CustomerValidatorRepository
     */
    private $repository;

    /**
     * @var ValidationFactory
     */
    private $validationFactory;

    /**
     * The constructor.
     *
     * @param CustomerValidatorRepository $repository The repository
     * @param ValidationFactory $validationFactory The validation
     */
    public function __construct(CustomerValidatorRepository $repository, ValidationFactory $validationFactory)
    {
        $this->repository = $repository;
        $this->validationFactory = $validationFactory;
    }

    /**
     * Create validator.
     *
     * @return Validator The validator
     */
    private function createValidator(): Validator
    {
        $validator = $this->validationFactory->createValidator();

        return $validator
            ->notEmptyString('name', 'Input required')
            ->notEmptyString('surname', 'Input required')
            ->notEmptyString('initialBalance', 'Input required')
            ->notEmptyString('legalId', 'Input required')
            ->notEmptyString('branchId', 'Input required');
    }

    /**
     * Validate new customer.
     *
     * @param array<mixed> $data The data
     *
     * @throws ValidationException
     *
     * @return void
     */
    public function validateCustomer(array $data): void
    {
        $validator = $this->createValidator();

        $validationResult = $this->validationFactory->createResultFromErrors(
            $validator->validate($data)
        );

        if ($validationResult->fails()) {
            throw new ValidationException('Please check your input', $validationResult);
        }
    }

    /**
     * Validate update.
     *
     * @param int $customerId The customer id
     * @param array<mixed> $data The data
     *
     * @return void
     */
    public function validateCustomerUpdate(int $customerId, array $data): void
    {
        if (!$this->repository->existsCustomerId($customerId)) {
            throw new ValidationException(sprintf('Customer not found: %s', $customerId));
        }

        $this->validateCustomer($data);
    }
}
