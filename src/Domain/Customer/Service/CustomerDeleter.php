<?php

namespace App\Domain\Customer\Service;

use App\Domain\Customer\Repository\CustomerDeleterRepository;

/**
 * Service.
 */
final class CustomerDeleter
{
    /**
     * @var CustomerDeleterRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param CustomerDeleterRepository $repository The repository
     */
    public function __construct(CustomerDeleterRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Delete user.
     *
     * @param int $userId The user id
     *
     * @return void
     */
    public function deleteCustomer(int $userId): void
    {
        // Input validation
        // ...

        $this->repository->deleteCustomerById($userId);
    }
}
