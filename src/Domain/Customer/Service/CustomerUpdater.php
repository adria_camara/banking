<?php

namespace App\Domain\Customer\Service;

use App\Domain\Customer\Repository\CustomerUpdaterRepository;
use App\Factory\LoggerFactory;
use Psr\Log\LoggerInterface;

/**
 * Service.
 */
final class CustomerUpdater
{
    /**
     * @var CustomerUpdaterRepository
     */
    private $repository;

    /**
     * @var CustomerValidator
     */
    private $customerValidator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * The constructor.
     *
     * @param CustomerUpdaterRepository $repository The repository
     * @param CustomerValidator $customerValidator The validator
     * @param LoggerFactory $loggerFactory The logger factory
     */
    public function __construct(
        CustomerUpdaterRepository $repository,
        CustomerValidator $customerValidator,
        LoggerFactory $loggerFactory
    ) {
        $this->repository = $repository;
        $this->customerValidator = $customerValidator;
        $this->logger = $loggerFactory
            ->addFileHandler('customer_update.log')
            ->createLogger();
    }

    /**
     * Update user.
     *
     * @param int $userId The user id
     * @param array<mixed> $data The request data
     *
     * @return void
     */
    public function updateCustomer(int $userId, array $data): void
    {
        // Input validation
        $this->customerValidator->validateCustomerUpdate($userId, $data);

        // Map form data to row
        $userRow = $this->mapToCustomerRow($data);

        // Insert user
        $this->repository->updateCustomer($userId, $userRow);

        // Logging
        $this->logger->info(sprintf('Customer updated successfully: %s', $userId));
    }

    /**
     * Map data to row.
     *
     * @param array<mixed> $data The data
     *
     * @return array<mixed> The row
     */
    private function mapToCustomerRow(array $data): array
    {
        $result = [];

        if(isset($data['branch_id']) && !empty($data['branch_id'])) {
            $result['branch_id'] = $data['branch_id'];
        }

        if(isset($data['name']) && !empty($data['name'])) {
            $result['name'] = $data['name'];
        }

        if(isset($data['surname']) && !empty($data['surname'])) {
            $result['surname'] = $data['surname'];
        }

        if(isset($data['legal_id']) && !empty($data['legal_id'])) {
            $result['legal_id'] = $data['legal_id'];
        }

        if(isset($data['balance']) && !empty($data['balance'])) {
            $result['balance'] = $data['balance'];
        }

        if(isset($data['initial_balance']) && !empty($data['initial_balance'])) {
            $result['initial_balance'] = $data['initial_balance'];
        }

        $result['updated_at'] = new \DateTime();

        return $result;
    }
}
