<?php

namespace App\Domain\Customer\Repository;

use Cake\Chronos\Chronos;
use PDO;

/**
 * Repository.
 */
final class CustomerUpdaterRepository
{
    /**
     * @var \PDO
     */
    private $connection;

    /**
     * CustomerUpdaterRepository constructor.
     * @param PDO $connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }


    public function updateCustomer(int $customerId, array $data): void
    {

        $data['updated_at'] = Chronos::now()->toDateTimeString();

        $set = "";

        foreach ( $data as $key => $value) {

            $set .= ($set != '') ? ", $key = :$key" : "$key = :$key";
        }

        $sql = "UPDATE customers SET $set WHERE id = :customerId";

        $stmt = $this->connection->prepare($sql);

        $stmt->bindValue(':customerId', $customerId);
        foreach ( $data as $key => $value ) {
            $stmt->bindValue(':'.$key, $value);
        }

        $stmt->execute();
    }
}
