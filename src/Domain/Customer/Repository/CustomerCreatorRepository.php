<?php

namespace App\Domain\Customer\Repository;

use PDO;
use Cake\Chronos\Chronos;

/**
 * Repository.
 */
final class CustomerCreatorRepository
{
    private $connection;


    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $row
     * @return int
     */
    public function insertCustomer(array $row): int
    {
        $sql= "INSERT INTO customers (branch_id, name, surname, initial_balance, balance, legal_id) 
            VALUES (:branch_id, :name, :surname, :initial_balance, :balance, :legal_id)";


        $stmt = $this->connection->prepare($sql);

        foreach ( $row as $key => $value) {
            $stmt->bindValue(':'.$key, $value);
        }

        $stmt->execute();

        return $this->connection->lastInsertId();
    }
}
