<?php

namespace App\Domain\Customer\Repository;

use PDO;

/**
 * Repository.
 */
final class CustomerFinderRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Load data table entries.
     *
     * @param array<mixed> $params The user
     *
     * @return array<mixed> The list view data
     */
    public function findCustomers(array $params): array
    {
        $sql = "SELECT * FROM customers";

        /** @var \PDOStatement $stmt */
        $stmt = $this->connection->query($sql);

        return $stmt->fetchAll();
    }

    public function findOtherCustomers(int $customerId): array
    {
        $sql = "SELECT * FROM customers where id <> :customerId";

        /** @var \PDOStatement $stmt */
        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam(':customerId', $customerId);

        $stmt->execute();

        return $stmt->fetchAll();
    }
}
