<?php

namespace App\Domain\Customer\Repository;

use PDO;
use DomainException;

/**
 * Repository.
 */
final class CustomerReaderRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get customer by id.
     *
     * @param int $customerId The customer id
     *
     * @throws DomainException
     *
     * @return array<mixed> The customer row
     */
    public function getCustomerById(int $customerId): array
    {
        $sql = "SELECT * FROM customers WHERE id = :id";

        /** @var \PDOStatement $stmt */
        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue(':id', $customerId);
        $stmt->execute();
        $row = $stmt->fetch();

        if (!$row) {
            throw new DomainException(sprintf('Customer not found: %s', $customerId));
        }

        return $row;
    }
}
