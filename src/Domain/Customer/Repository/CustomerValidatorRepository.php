<?php

namespace App\Domain\Customer\Repository;

use App\Domain\Customer\Service\CustomerReader;
use App\Factory\QueryFactory;

/**
 * Repository.
 */
final class CustomerValidatorRepository
{
    /**
     * @var QueryFactory The query factory
     */
    private $customerReader;


    public function __construct(CustomerReader $customerReader)
    {
        $this->customerReader = $customerReader;
    }

    /**
     * Check customer id.
     *
     * @param int $customerId The customer id
     *
     * @return bool True if exists
     */
    public function existsCustomerId(int $customerId): bool
    {
        return (bool)$this->customerReader->getCustomerData($customerId);
    }
}
